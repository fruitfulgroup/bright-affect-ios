//
//  CourseTableCell.swift
//  Bright Affect
//
//  Created by Zee on 28/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit

class CourseTableCell: UITableViewCell {
    
    @IBOutlet var iv_photo: UIImageView!
    
    @IBOutlet var l_title: UILabel!
    @IBOutlet var l_subtitle: UILabel!
    @IBOutlet var tv_overview: UITextView!
    
    @IBOutlet var l_offline: UILabel!

}
