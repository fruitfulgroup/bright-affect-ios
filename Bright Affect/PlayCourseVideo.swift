//
//  PlayCourseVideo.swift
//  Bright Affect
//
//  Created by Zee on 28/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit
import CoreData
import MediaPlayer

class PlayCourseVideo: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    var course:NSManagedObject!
    var photo:UIImage!
    var videos:[NSManagedObject]!
    var seenVideos: Array<Bool> = []
    var saveOffline = NSMutableDictionary()
    var courseID: Int = 0
    
    @IBOutlet var b_playButton: UIButton!
    @IBOutlet var iv_coursePhoto: UIImageView!
    @IBOutlet var l_title: UILabel!
    @IBOutlet var tv_script: UITextView!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var v_VideoView: UIView!
    @IBOutlet var sw_Offline: UISwitch!
    
    var moviePlayer:MPMoviePlayerController = MPMoviePlayerController()
    var downloadingAlert: UIAlertView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imgView = UIImageView(image: UIImage(named: "evo logo long.png"))
        imgView.contentMode = .ScaleAspectFit
        imgView.frame = CGRect(x: imgView.frame.origin.x, y: imgView.frame.origin.y, width: imgView.frame.width, height: 20)
        
        self.navigationItem.titleView = imgView
        self.navigationItem.backBarButtonItem?.title = "  "
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
        self.tableview.selectRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0) , animated: false, scrollPosition: UITableViewScrollPosition.None)
        for vids in videos {  seenVideos.append(false) }
        
        if let y = course.valueForKey("title") as? String { self.navigationItem.title = y }
        
        if let p = photo { iv_coursePhoto.image = p }        

        courseID = (course.valueForKey("course_id") as? Int)!
        
        if let download:Bool = self.saveOffline.valueForKey("\(courseID)") as? Bool {
            sw_Offline.on = download
        }
        
        sw_Offline.addTarget(self, action: Selector("stateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        /*
        let alertController = UIAlertController(title: "Title", message: "Message", preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        
        let testView = UIViewController()
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 200))
        view.backgroundColor = UIColor.redColor()
        testView.view = view
        

        alertController.addChildViewController(testView)
            
        presentViewController(alertController, animated: true, completion: nil)
        */
        
        
        
        
        downloadingAlert = UIAlertView(title: FLocal.LocalizedString("DOWNLOADING", comment: ""), message: nil, delegate: nil, cancelButtonTitle: nil)
        
        var loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(50, 10, 37, 37)) as UIActivityIndicatorView
        loadingIndicator.center = self.view.center;
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        loadingIndicator.startAnimating();
        
        downloadingAlert.setValue(loadingIndicator, forKey: "accessoryView")
        
        
        //Get video from index
        if videos.count > 0 {
            
            //Set Text information
            let obj = videos[0]
            if let y:String = obj.valueForKey("title") as? String { l_title.text = y }
            if let y:String = obj.valueForKey("script") as? String { tv_script.text = y }
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "MPMoviePlayerPlaybackStateDidChange:", name: MPMoviePlayerPlaybackStateDidChangeNotification, object: nil)
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

        
        if self.isMovingFromParentViewController() || self.isBeingDismissed() {
            moviePlayer.stop()
            self.dismissMoviePlayerViewControllerAnimated()
        }
    }
    
 
    func MPMoviePlayerPlaybackStateDidChange(notification: NSNotification){
            
        if moviePlayer.playbackState == MPMoviePlaybackState.Stopped {
            moviePlayer.setFullscreen(false, animated: false)
            PlaySelectedVideo()
        }
        else{
            
        }
        
    }
    
    
     @IBAction func Play(sender: UIButton) {
        
        self.iv_coursePhoto.hidden = true
        self.b_playButton.hidden = true
        
        PlaySelectedVideo()
    }
    
    func PlaySelectedVideo() {
        var index = self.tableview.indexPathForSelectedRow()
        seenVideos[index!.row] = true
        moviePlayer.stop()
        self.dismissMoviePlayerViewControllerAnimated()
        self.tableview.reloadData()
        self.tableview.selectRowAtIndexPath(index, animated: true, scrollPosition: UITableViewScrollPosition.None)
        
        //Get video from index
        let obj = videos[index!.row]
        
        //Set Text information
        if let y:String = obj.valueForKey("title") as? String { l_title.text = y }
        if let y:String = obj.valueForKey("script") as? String { tv_script.text = y }
        
        
        if let vid:String = obj.valueForKey("video") as? String {
            
            var videoURL:NSURL!
            var videoType:MPMovieSourceType!
            
            
            if Fruitful.FileExists(Filename: vid, Path: "/Videos/") {
                
                if let url = NSURL(fileURLWithPath: "\(Fruitful.GetFilePath())/Videos/\(vid)" ) {
                    moviePlayer = MPMoviePlayerController(contentURL: url)
                    moviePlayer.movieSourceType = MPMovieSourceType.File
                }
                else {
                    return
                }
            }
            else {
                
                if let url = NSURL( string: obj.valueForKey("video_link") as! String ) {
                    
                    if url.checkResourceIsReachableAndReturnError(nil) == false {
                        moviePlayer = MPMoviePlayerController(contentURL: url)
                        moviePlayer.movieSourceType = MPMovieSourceType.Unknown
                    }
                }
                else{
                    return
                }
                
            }
            
            moviePlayer.view.frame = v_VideoView.frame
            moviePlayer.view.sizeToFit()
            moviePlayer.scalingMode = MPMovieScalingMode.AspectFit
            moviePlayer.fullscreen = false
            moviePlayer.controlStyle = MPMovieControlStyle.Embedded
            moviePlayer.repeatMode = MPMovieRepeatMode.None
            moviePlayer.play()
            v_VideoView.addSubview( moviePlayer.view )
            
        }
        
    }
    
    
    func stateChanged(switchState: UISwitch) {
        
        
        if switchState.on {
            var alert = UIAlertController(title: FLocal.LocalizedString("SAVE_OFFLINE", comment: ""), message: FLocal.LocalizedString("SAVE_OFFLINE_TEXT", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("NO", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                
                switchState.on = false
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("YES", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                self.saveOffline.setValue(true, forKey: "\(self.courseID)")
                self.DownloadVideos()
                alert.dismissViewControllerAnimated(true, completion: nil)
                
            }))
        
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            var alert = UIAlertController(title: FLocal.LocalizedString("REMOVE_OFFLINE", comment: ""), message: FLocal.LocalizedString("REMOVE_OFFLINE_TEXT", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("NO", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: FLocal.LocalizedString("YES", comment: ""), style: UIAlertActionStyle.Default, handler: { alertAction in
                self.saveOffline.setValue(false, forKey: "\(self.courseID)")
                var standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                standardUserDefaults.setObject(self.saveOffline, forKey: "offline")
                NSUserDefaults.standardUserDefaults().synchronize()
            
                //Delete all videos
                for vid in self.videos {
                    let videoName = vid.valueForKey("video") as! String
                    Fruitful.DeleteFile(Filename: videoName, Path: "/Videos/")
                }
                
                switchState.on = false
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    func DownloadVideos() {
        
        let priority = DISPATCH_QUEUE_PRIORITY_LOW
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            dispatch_async(dispatch_get_main_queue(), {
                self.downloadingAlert.show()
            })
            
            for vid in self.videos {
                let videoName = vid.valueForKey("video") as! String
                let videolink = vid.valueForKey("video_link") as! String
                
                
                if !Fruitful.FileExists(Filename: videoName, Path: "/Videos/"){
                                
                    if let url = NSURL(string: videolink ) {
                        if let data:NSData = NSData(contentsOfURL: url){
                                        
                            Fruitful.SaveFile(data, Filename: videoName, Path: "/Videos/")
                            println("Downloaded: \(videoName)")
                        }
                    }
                }
                                
            }
                
            dispatch_async(dispatch_get_main_queue(), {
                self.downloadingAlert.dismissWithClickedButtonIndex(0, animated: true)
                
                var standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                standardUserDefaults.setObject(self.saveOffline, forKey: "offline")
                NSUserDefaults.standardUserDefaults().synchronize()
                
            })
            
        }
    }
    

    
    
    

            
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return videos.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("VideoCell", forIndexPath: indexPath) as! UITableViewCell
        
        let vid = videos[indexPath.row]
        
        if let y = vid.valueForKey("title") as? String { cell.textLabel!.text = y }
        
        cell.accessoryType = seenVideos[indexPath.row]  ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if iv_coursePhoto.hidden {
            PlaySelectedVideo()
        }
    }


    
}
