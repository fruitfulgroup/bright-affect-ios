//
//  ViewController.swift
//  Bright Affect
//
//  Created by Zee on 19/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit

class MainMenuView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackToLanguageSelect(sender: UIButton) {
        var standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        standardUserDefaults.removeObjectForKey("LangID")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    @IBAction func Options(sender: UIButton) {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let userAction = UIAlertAction(title: FLoginControl.UserName(), style: .Default, handler: { (alert: UIAlertAction!) -> Void in  })
        
        let logoutAction = UIAlertAction(title: FLocal.LocalizedString("LOGOUT", comment: ""), style: .Destructive, handler: { (alert: UIAlertAction!) -> Void in
            FLoginControl.Logout()
            self.navigationController?.popViewControllerAnimated(true)
            
        })
        
        optionMenu.addAction(userAction)
        optionMenu.addAction(logoutAction)
        
        
        if let v = sender.valueForKey("view") as? UIView {
            var pop:UIPopoverController = UIPopoverController(contentViewController: optionMenu)
            pop.setPopoverContentSize(optionMenu.view.frame.size, animated: false)
            pop.presentPopoverFromRect( CGRect(origin: v.frame.origin, size: CGSize()), inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
            
        }
        
    }



}

