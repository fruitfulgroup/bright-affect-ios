//
//  JSONUrlRequest.swift
//  FruitfulLibraries
//
//  Created by Gaurav Ravindra on 01/05/2015.
//  Copyright (c) 2015 Fruitful Business Services. All rights reserved.
//

import Foundation
import UIKit


public class JSONUrlRequest {

    var request:NSMutableURLRequest
    var body:NSMutableData
    public var ParsedObjects:NSMutableArray
    
    private var alert_Refreshing:UIAlertView
    public var OnRequestBegin:(() -> ())?
    public var OnRequestEnd:(() -> ())?
    public var OnRequestSuccess:((result:String) -> ())?
    public var OnRequestFail:((result:String) -> ())?
    
    public var CustomRefreshView:(UIView)?
    
    let API_BOUNDARY:(String) = "--StopStiffingOurPostDataYouBanana"
    
    //private let quene = dispatch_queue_create("serial-worker", DISPATCH_QUEUE_CONCURRENT)
    

    init() {
        self.request = NSMutableURLRequest()
        self.body = NSMutableData()
        self.request.HTTPMethod = "POST"
        self.ParsedObjects = NSMutableArray()
        request.addValue("multipart/form-data; boundary=\(API_BOUNDARY)", forHTTPHeaderField: "Content-Type")
        

        self.alert_Refreshing = UIAlertView(title: "Please Wait...", message: nil, delegate: nil, cancelButtonTitle: nil)
        
        var indicator:(UIActivityIndicatorView) = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        indicator.center = CGPointMake(alert_Refreshing.bounds.size.width / 2, alert_Refreshing.bounds.size.height - 50);
        indicator.startAnimating()
        self.alert_Refreshing.addSubview(indicator)
    }

    func SetURL(#UrlString:String) {
        self.request.URL = NSURL(string: UrlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
    }
    
    func AddParameter(Name n:String, Value v:String){
        self.body.appendData(String("--\(API_BOUNDARY)\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("Content-Disposition: form-data; name=\"\(n)\"\r\n\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("\(v)").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
    }
    
    func AddFile(Name n:String, Data d:NSData){
        self.body.appendData(String("--\(API_BOUNDARY)\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("Content-Disposition: form-data; name=\"\(n)\"; filename=\"%@\"\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("Content-Type: application/octet-stream\r\n\r\n\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(d)
        self.body.appendData(String("\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
    }
    
    func GetRequest() -> NSMutableURLRequest {
        
        var newBody:(NSMutableData) = NSMutableData(data: self.body)
        newBody.appendData(String("--\(API_BOUNDARY)\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        
        var newRequest:(NSMutableURLRequest) = NSMutableURLRequest()
        newRequest.HTTPMethod = "POST"
        newRequest.URL = self.request.URL
        newRequest.addValue("multipart/form-data; boundary=\(API_BOUNDARY)", forHTTPHeaderField: "Content-Type")
        
        var dataString:String = NSString(data: newBody, encoding: NSUTF8StringEncoding) as! String
        
        #if DEBUG
            println("Request URL: \(newRequest.URL)")
            println("Request DATA: \(dataString)")
        #endif
        
        newRequest.HTTPBody = newBody
        return newRequest
    }
    
    
    func GetSingleObjectFromUrlRequest(#ShowAlert:Bool){
        
        let priority = DISPATCH_QUEUE_PRIORITY_HIGH
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
        //dispatch_async(quene) {
            
            dispatch_async(dispatch_get_main_queue(), {
                if(ShowAlert){ self.ShowRefreshAlert() }
                if let v = self.CustomRefreshView { v.hidden = false }
                if let y = self.OnRequestBegin { self.OnRequestBegin!() }
            })
            
            var v = self._getJson()
            
            if(v.sucess) {
                if let y = self.OnRequestSuccess { self.OnRequestSuccess!(result: v.text) }
            }
            else{
                if let y = self.OnRequestFail { self.OnRequestFail!(result: v.text) }
            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                if(ShowAlert){ self.DismissRefreshAlert() }
                if let v = self.CustomRefreshView { v.hidden = true }
                if let y = self.OnRequestEnd { self.OnRequestEnd!() }
            })
        }
        
    }
    
    
    func GetJsonFromUrlRequest(#ShowAlert:Bool){
        let priority = DISPATCH_QUEUE_PRIORITY_HIGH
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
        //dispatch_async(quene) {
            
            dispatch_async(dispatch_get_main_queue(), {
                if(ShowAlert){ self.ShowRefreshAlert() }
                if let v = self.CustomRefreshView { v.hidden = false }
                if let y = self.OnRequestBegin { self.OnRequestBegin!() }
            })
            
            var v = self._getJsonArray()
            
            if(v.sucess) {
                if let y = self.OnRequestSuccess { self.OnRequestSuccess!(result: v.text) }
            }
            else{
                if let y = self.OnRequestFail { self.OnRequestFail!(result: v.text) }
            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                if(ShowAlert){ self.DismissRefreshAlert() }
                if let v = self.CustomRefreshView { v.hidden = true }
                if let y = self.OnRequestEnd { self.OnRequestEnd!() }
            })
        }
    }
    
    
    func GetStringFromUrlRequest(#ShowAlert:Bool){
        
        let priority = DISPATCH_QUEUE_PRIORITY_HIGH
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
        //dispatch_async(quene) {
            
            dispatch_async(dispatch_get_main_queue(), {
                if(ShowAlert){ self.ShowRefreshAlert() }
                if let v = self.CustomRefreshView { v.hidden = false }
                if let y = self.OnRequestBegin { self.OnRequestBegin!() }
            })
            
            var v = self._getString()
            
            if(v.sucess) {
                if let y = self.OnRequestSuccess { self.OnRequestSuccess!(result: v.text) }
            }
            else{
                if let y = self.OnRequestFail { self.OnRequestFail!(result: v.text) }
            }
            
            
            dispatch_async(dispatch_get_main_queue(), {
                if(ShowAlert){ self.ShowRefreshAlert() }
                if let v = self.CustomRefreshView { v.hidden = false }
                if let y = self.OnRequestEnd { self.OnRequestEnd!() }
            })
        
        }
    }
    
    
    
    private func _getJson() -> (sucess:Bool, text:String) {
       
        let req:NSMutableURLRequest = self.GetRequest()
        
        if let data:NSData = NSURLConnection.sendSynchronousRequest(req, returningResponse: nil, error: nil) {
    
            #if DEBUG
            if let returnString:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String{
                println("\(returnString)")
            }
            #endif
            
            if let json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: nil) as? NSDictionary{
                
                if let text:(String) = json.objectForKey("error") as? String {
                    return (false, text)
                }
                
                //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
                ParsedObjects.removeAllObjects()
                ParsedObjects.addObject(json)
                return (true, "JSON returned")
            }
            else{
                return (false, "No JSON was returned")
            }
            
        }
        else{
            return (false, "Unable to connect to database. Please check you have internet connection.")
        }
        
    }
    
    private func _getJsonArray() -> (sucess:Bool, text:String) {
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        if let data:NSData = NSURLConnection.sendSynchronousRequest(req, returningResponse: nil, error: nil) {
            
            #if DEBUG
            if let returnString:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String{
                println("\(returnString)")
            }
            #endif
            
            if let json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: nil) as? NSDictionary{
                
                if let text:(String) = json.objectForKey("error") as? String {
                    return (false, text)
                }
                else{
                    return (false, "No JSON was returned")
                }
            }
                
            if let allObjects:NSArray = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: nil) as? NSArray {
                //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
                ParsedObjects.removeAllObjects()
                ParsedObjects.addObjectsFromArray(allObjects as [AnyObject])
                return (true, "JSON returned")
            }
            else{
                return (false, "No JSON was returned")
            }
                
        }
        else{
            return (false, "Unable to connect to database. Please check you have internet connection.")
        }
        
    }



    private func _getString() -> (sucess:Bool, text:String) {
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        if let data:NSData = NSURLConnection.sendSynchronousRequest(req, returningResponse: nil, error: nil){
            
            
            var returnString:NSString = NSString(data: data, encoding: NSUTF8StringEncoding)!
            
            #if DEBUG
            println("\(returnString)")
            #endif
            
            if let json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: nil) as? NSDictionary {
                
                if let text:String = json.objectForKey("error") as? String{
                    return (false, text)
                }
            }
            
            /*
            var dic:NSDictionary = NSDictionary()
            dic.setValue(returnString, forKey: "Text")
            
            //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
            ParsedObjects.removeAllObjects()
            ParsedObjects.addObject(dic)
            */
            
            return (true, String(returnString))

        }
        else{
            return (false, "Unable to connect to database. Please check you have internet connection.")
        }
        
        
    }
    
    
    private func ShowRefreshAlert() {
        alert_Refreshing.show()
    }
    
    
    private func DismissRefreshAlert() {
        alert_Refreshing.dismissWithClickedButtonIndex(0, animated: true)
    }
    


}


