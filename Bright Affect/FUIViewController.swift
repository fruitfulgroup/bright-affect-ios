//
//  FUIViewController.swift
//  FruitfulLibraries
//
//  Created by Zee on 07/05/2015.
//  Copyright (c) 2015 Fruitful Business Services. All rights reserved.
//

import Foundation
import UIKit


public class FUIViewController : UIViewController {
    
    public override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
        
}
