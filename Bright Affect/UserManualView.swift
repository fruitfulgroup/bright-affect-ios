//
//  UserManualView.swift
//  Bright Affect
//
//  Created by Zee on 08/06/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit

class UserManualView: UIViewController {

    @IBOutlet var webView: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem?.title = ""
        if let html = NSBundle.mainBundle().pathForResource("index", ofType: "html", inDirectory: FLocal.LocalizedString("MANUAL_DIC", comment: "")) {
            let url = NSURL(fileURLWithPath: html, isDirectory: false)
            webView.loadRequest(NSURLRequest(URL: url!))
        }
        
    }
    
}
