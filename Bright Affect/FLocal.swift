//
//  FLocal.swift
//  Bright Affect
//
//  Created by Zee on 08/06/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import Foundation
import UIKit


public class FLocal {
    
    private static let _instance:FLocal = FLocal()
    private var langBundle:NSBundle = NSBundle.mainBundle()
    
    
    
    class func InitLocalWithCode(Code code:String) {
        
        if let appDel = UIApplication.sharedApplication().delegate {
            if let path = NSBundle.mainBundle().pathForResource(code, ofType: "lproj") {            
                if let b = NSBundle(path: path) {
                    _instance.langBundle = b
                }
            }
        }
        
    }
    
    
    class func LocalizedString(key:String, comment c: String = "") -> String{
        
        return _instance.langBundle.localizedStringForKey(key, value: key, table: nil)
    }
    
    
}


extension UILabel {
    
    var localizedText: String {
        set (key) { text = FLocal.LocalizedString(key)  }
        get { return text!  }
    }
    
}

extension UITextField {
    
    var localizedText: String {
        set (key) { text = FLocal.LocalizedString(key, comment: "")  }
        get { return text!  }
    }
    
    var localizedPlaceholder: String {
        set (key) { placeholder = FLocal.LocalizedString(key, comment: "")  }
        get { return placeholder!  }
    }
}


extension UIButton {
    var localizedTitle: String {
        set (key) {
            setTitle(FLocal.LocalizedString(key, comment: ""), forState: .Normal)
            setTitle(FLocal.LocalizedString(key, comment: ""), forState: .Highlighted)
            setTitle(FLocal.LocalizedString(key, comment: ""), forState: .Disabled)
        }
        get { return titleForState(.Normal)! }
    }
}