//
//  LanguageView.swift
//  Bright Affect
//
//  Created by Zee on 20/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit
import CoreData

class LanguageView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        var standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        var pref = NSLocale.preferredLanguages()[0].stringValue
        var path = NSBundle.mainBundle().pathForResource("en" , ofType: "lproj")
        
        
        if let y = standardUserDefaults.objectForKey("LangID") as? Int{
            
            GoToLogin(y, animated: false)
        }
    }
    
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func LanguageSelected(sender: UIButton) {
        
        var standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let y = standardUserDefaults.objectForKey("PreLangID")?.integerValue {
            
            if y != sender.tag {
                Fruitful.DeleteAllFiles(Path: "/Videos/")
                Fruitful.DeleteAllFiles(Path: "/Photos/")
                
                var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                if let managedContext:NSManagedObjectContext = appDelegate.managedObjectContext {
                
                    //Clear/Delete old video data first
                    let fetchRequest = NSFetchRequest(entityName: "Courses")
                    if let oldCourses = managedContext.executeFetchRequest(NSFetchRequest(entityName: "Courses"), error: nil) as? [NSManagedObject] {
                        for course:NSManagedObject in oldCourses {
                            managedContext.deleteObject(course)
                        }
                    }
                
                    if let oldVids = managedContext.executeFetchRequest(NSFetchRequest(entityName: "Videos"), error: nil) as? [NSManagedObject] {
                        for vid:NSManagedObject in oldVids {
                            managedContext.deleteObject(vid)
                        }
                    }
                
                    managedContext.save(nil)
                    
                }

            }
        }
        
        GoToLogin(sender.tag, animated: true)

    }
    
    private func GoToLogin(LangID: Int, animated: Bool) {
        var standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        Fruitful.LANGUAGE_ID = LangID
        
        switch(LangID) {
            
            case 1: FLocal.InitLocalWithCode(Code: "en"); break
            case 2: FLocal.InitLocalWithCode(Code: "fr"); break
            case 3: FLocal.InitLocalWithCode(Code: "es"); break
            case 5: FLocal.InitLocalWithCode(Code: "bg"); break
            case 6: FLocal.InitLocalWithCode(Code: "el"); break
            case 7: FLocal.InitLocalWithCode(Code: "ko"); break
            default: FLocal.InitLocalWithCode(Code: ""); break
        }
        
        
        standardUserDefaults.setObject(LangID, forKey: "LangID");
        standardUserDefaults.setObject(LangID, forKey: "PreLangID");
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        if let view = self.storyboard?.instantiateViewControllerWithIdentifier("MainMenu") as? MainMenuView {
            self.navigationController?.pushViewController(view, animated: animated)
        }
        
        /*
        if let view = self.storyboard?.instantiateViewControllerWithIdentifier("LoginView") as? LoginView {
            self.navigationController?.pushViewController(view, animated: animated)
        }
        */

    }
    
}
